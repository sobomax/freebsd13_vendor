/*-
 * This file is in the public domain.
 */
/* $FreeBSD: 95b5c274ff1ee71b9ac220ada26dfc232a91e1ca $ */

#ifndef	_MACHINE_IOMMU_H_
#define	_MACHINE_IOMMU_H_

#include <arm64/iommu/iommu.h>

#endif /* !_MACHINE_IOMMU_H_ */
