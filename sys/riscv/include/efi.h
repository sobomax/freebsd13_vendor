/*-
 * This file is in the public domain since it's just boilerplate.
 *
 * $FreeBSD: 4f1e2181b877ddf8df7b801c3592f5dc8c8e9783 $
 */

#ifndef __RISCV_INCLUDE_EFI_H_
#define __RISCV_INCLUDE_EFI_H_

#define	EFIABI_ATTR

/* Note: we don't actually support this on riscv */

#endif /* __I386_INCLUDE_EFI_H_ */
