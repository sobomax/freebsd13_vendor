/*-
 * This file is in the public domain since it's just boilerplate.
 *
 * $FreeBSD: 8c52da2a6e6c27d631ab5a707044a9241355e39d $
 */

#ifndef __MIPS_INCLUDE_EFI_H_
#define __MIPS_INCLUDE_EFI_H_

#define	EFIABI_ATTR

/* Note: we don't actually support this on mips */

#endif /* __MIPS_INCLUDE_EFI_H_ */
