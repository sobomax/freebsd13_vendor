/*-
 * This file is in the public domain.
 */
/* $FreeBSD: 718559e8b893322b08a893eb85610e20e5bb4531 $ */

#ifndef _SYS__SEQC_H_
#define _SYS__SEQC_H_

typedef uint32_t seqc_t;

#endif /* _SYS__SEQC_H */
