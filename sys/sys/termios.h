/* $FreeBSD: e5bacf46550fb6553d790d350b2cad918695fb64 $ */
#ifdef __GNUC__
#warning "this file includes <sys/termios.h> which is deprecated, use <termios.h> instead"
#endif
#include <termios.h>
