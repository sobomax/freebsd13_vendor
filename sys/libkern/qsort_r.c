/*-
 * This file is in the public domain.  Originally written by Garrett
 * A. Wollman.
 *
 * $FreeBSD: 00f9f056048df76ace42a3d4c39b0fe0511eebaf $
 */
#define I_AM_QSORT_R
#include "libkern/qsort.c"
