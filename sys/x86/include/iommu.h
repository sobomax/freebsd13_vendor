/*-
 * This file is in the public domain.
 */
/* $FreeBSD: 2757f1554258e4e863fce490ffe777f47d0ba09a $ */

#ifndef	_MACHINE_IOMMU_H_
#define	_MACHINE_IOMMU_H_

#include <x86/include/busdma_impl.h>
#include <x86/iommu/intel_reg.h>
#include <x86/iommu/intel_dmar.h>

#endif /* !_MACHINE_IOMMU_H_ */
