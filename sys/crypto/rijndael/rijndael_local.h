/*	$KAME: rijndael_local.h,v 1.5 2003/08/28 08:37:24 itojun Exp $	*/
/*	$FreeBSD: 1954fec65c6a66742aa272ab229b4739f95b86de $	*/

/* the file should not be used from outside */
typedef uint8_t			u8;
typedef uint16_t		u16;
typedef uint32_t		u32;
