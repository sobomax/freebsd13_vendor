/* $FreeBSD: ee4d93db1947423cf32bc4e40e1fc068c71741ff $ */
#ifndef VERSION_HIDDEN
#define VERSION_HIDDEN
#endif
VERSION_HIDDEN const char *heimdal_long_version = "@(#)$Version: Heimdal 1.5.2 (FreeBSD) $";
VERSION_HIDDEN const char *heimdal_version = "Heimdal 1.5.2";
