/* $FreeBSD: 5557de8f735690f1774453010b8d838a9c147ef3 $ */
#ifdef __GNUC__
#warning "this file includes <gssapi.h> which is deprecated, use <gssapi/gssapi.h> instead"
#endif
#include <gssapi/gssapi.h>
