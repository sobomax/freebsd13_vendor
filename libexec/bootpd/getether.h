/* getether.h */
/* $FreeBSD: 026df158b50ce981cc72d61da4ff4cf28ddb8bf7 $ */

extern int getether(char *ifname, char *eaptr);
