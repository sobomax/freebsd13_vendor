/* getif.h */
/* $FreeBSD: a2c86cfba04b69d4e74c9bff37f14851ebf3a826 $ */

extern struct ifreq *getif(int, struct in_addr *);
