/*-
 * Placed in the public domain by Todd C. Miller <Todd.Miller@courtesan.com>
 * on July 29, 2003.
 *
 * $OpenBSD: pathnames.h,v 1.1 2003/07/29 20:10:17 millert Exp $
 * $FreeBSD: 79d8faea62859a6fdd8543670b554689ffbbb18f $
 */


#include <paths.h>

#define	_PATH_RED		"/bin/red"
