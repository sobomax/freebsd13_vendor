#!/bin/sh
# $FreeBSD: d9c3127e43d625a073b8e0c22f658dab62ce3585 $
# This file is in the public domain.
builtin ${0##*/} ${1+"$@"}
