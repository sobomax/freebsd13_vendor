#!/bin/sh
# $FreeBSD: 45faeec0ec45f191626dc145e55e142e898d03ac $
# This file is in the public domain.
exec /usr/bin/c++ "$@"
