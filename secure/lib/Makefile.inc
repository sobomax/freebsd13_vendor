# $FreeBSD: c6d2f5ba464e61757f2a2796a940191ed5ca8312 $

.include "../Makefile.inc"
.if exists(${.CURDIR:H:H:H}/lib/Makefile.inc)
.include "${.CURDIR:H:H:H}/lib/Makefile.inc"
.endif
