/* $FreeBSD: 33eea10f2a3cdba430c60ed7363f3f4ed1ee80ce $ */
#include <unistd.h>

static uint32_t buf[512/4];
main()
{
	u_int u = 0;

	while (1) {
		buf[0] = u++;

		if (512 != write(1, buf, sizeof buf))
			break;
	}
	exit (0);
}
