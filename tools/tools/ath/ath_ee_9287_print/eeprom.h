/* $FreeBSD: a5fc76a9b58d9fa3a5dee17a2fb991559b2c9a53 $ */

#ifndef	__EEPROM_H__
#define	__EEPROM_H__

extern void load_eeprom_dump(const char *file, uint16_t *buf);

#endif
