/* $FreeBSD: 8332092a36ad418faeeed4f5aff920a4f8f98310 $ */

#ifndef _CRT_H_
#define _CRT_H_

#define	HAVE_CTORS
#define	INIT_CALL_SEQ(func)	"call " __STRING(func)

#endif
