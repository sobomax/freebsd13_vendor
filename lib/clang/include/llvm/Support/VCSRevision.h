/* $FreeBSD: f2f1c75acb50d649e80833135da177818852bfaf $ */
#define LLVM_REVISION "llvmorg-14.0.5-0-gc12386ae247c"
#define LLVM_REPOSITORY "https://github.com/llvm/llvm-project.git"
