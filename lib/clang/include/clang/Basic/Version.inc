/* $FreeBSD: d0c3b2db52c3d9f1ed5febb69cdb82ac529771ae $ */

#define	CLANG_VERSION			14.0.5
#define	CLANG_VERSION_STRING		"14.0.5"
#define	CLANG_VERSION_MAJOR		14
#define	CLANG_VERSION_MINOR		0
#define	CLANG_VERSION_PATCHLEVEL	5

#define	CLANG_VENDOR			"FreeBSD "
