#include <sys/cdefs.h>
__FBSDID("$FreeBSD: a757ded233a3f0579142bb742f6840f217a27b32 $");

#define type		float
#define	roundit		rintf
#define dtype		long
#define	fn		lrintf

#include "s_lrint.c"
