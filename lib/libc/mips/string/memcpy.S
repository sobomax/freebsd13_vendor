/*	$NetBSD: memcpy.S,v 1.1 2005/12/20 19:28:50 christos Exp $	*/

#include <machine/asm.h>
__FBSDID("$FreeBSD: 8d3c0dbfcf2c40e0e54adb46730f7d9425c43262 $");

#define MEMCOPY
#include "bcopy.S"
