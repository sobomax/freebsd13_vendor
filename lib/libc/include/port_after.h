/* $FreeBSD: 94f3a6431e98ffc642aef34d89bcefe82c5e9781 $ */

#ifndef _PORT_AFTER_H_
#define _PORT_AFTER_H_

#define HAVE_SA_LEN		1
#define HAS_INET6_STRUCTS	1
#define HAVE_SIN6_SCOPE_ID	1
#define HAVE_TIME_R		1

#endif /* _PORT_AFTER_H_ */
