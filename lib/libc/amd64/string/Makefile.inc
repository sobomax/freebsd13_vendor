# $FreeBSD: cb370bc6be1cd02771a0a2b6f01fc819b0f66b91 $

MDSRCS+= \
	bcmp.S \
	memcmp.S \
	memcpy.S \
	memmove.S \
	memset.S \
	strcat.S \
	strcmp.S \
	strlen.S \
	stpcpy.S
