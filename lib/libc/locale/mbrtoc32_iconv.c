/* $FreeBSD: ec2c0145d9d66a291fef2f410c9983e0832a4d24 $ */
#define	charXX_t	char32_t
#define	mbrtocXX	mbrtoc32
#define	mbrtocXX_l	mbrtoc32_l
#define	DSTBUF_LEN	1
#define	UTF_XX_INTERNAL	"UTF-32-INTERNAL"

#include "mbrtocXX_iconv.h"
