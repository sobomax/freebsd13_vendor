/* $FreeBSD: f1eaf1925496dc78ff7e1561033c34b9f9b7bd51 $ */
#define	charXX_t	char16_t
#define	mbrtocXX	mbrtoc16
#define	mbrtocXX_l	mbrtoc16_l
#define	DSTBUF_LEN	2
#define	UTF_XX_INTERNAL	"UTF-16-INTERNAL"

#include "mbrtocXX_iconv.h"
