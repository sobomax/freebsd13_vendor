# $FreeBSD: a8c50a285cbd6d9bca2bde91347a1aa361885090 $

MDSRCS+= \
	bcmp.S \
	bcopy.S \
	bzero.S \
	ffs.S \
	memcmp.S \
	memcpy.S \
	memmove.S \
	memset.S \
	strcat.S \
	strchr.S \
	strcmp.S \
	strcpy.S \
	strncmp.S \
	strrchr.S \
	swab.S \
	wcschr.S \
	wcscmp.S \
	wcslen.S \
	wmemchr.S
