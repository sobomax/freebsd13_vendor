/* $FreeBSD: a0920e2724bca88ef22d251b14ecfff0d3af3b62 $ */

#include <sys/types.h>
#include <libelftc.h>

const char *
elftc_version(void)
{
	return "elftoolchain r3769";
}
