/* $FreeBSD: c35c721aa83847bfed4c188880a1e5b9c9fdf71f $ */
#include <paths.h>

#define	_PATH_DEVPCI	"/dev/pci"
#define	_PATH_PCIVDB	"/usr/share/misc/pci_vendors"
#define	_PATH_LPCIVDB	_PATH_LOCALBASE "/share/pciids/pci.ids"
